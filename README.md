# Array_Object_practice

### ARRAY_PRACTICE

##### Input


```
const nestedArray = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
  [10, 11, 12]
];
```


##### Function code
```
const iteratingArray = ( array ) => {

  for ( const element of array ) {
     Array.isArray(element) ? iteratingArray(element) : console.log(element);
  }

}

iteratingArray(nestedArray);
```


##### Output 
```
1
2
3
4
5
6
7
8
9
10
11
12
```


### Object Practice


##### Input


```
const nestedObject = {
    person1: {
      name: "Alice",   
      age: 28,
      address: {
        city: "Wonderland",
        country: "Fantasyland"
      }
    },
    person2: {
      name: "Bob",
      age: 35,
      address: {
        city: "Techville",
        country: "Coderland"
      }
    },
    person3: {
      name: "Charlie",
      age: 22,
      address: {
        city: "Artstown",
        country: "Creativia"
      }
    }
};
```


#####  Function


```
const iterateObject = (object) => {
    for ( const key1 in object ) {
        (object[key1] instanceof Object) ? iterateObject(object[key1]) : console.log(object[key1]);
    }
}
```


##### Output


```
Alice
28
Wonderland
Fantasyland
Bob
35
Techville
Coderland
Charlie
22
Artstown
Creativia
```
